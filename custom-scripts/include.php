<?php

// County value
$counties = array(
    'BROWARD' => 'Broward',
    'DADE' => 'Miami-Dade',
    'PALMBCH' => 'Palm Beach'
);

// Cities per County
$PALMBCH = array(
    'Atlantis',
    'Belle Glade',
    'Boca Raton',
    'Boynton Beach',
    'Delray Beach',
    'Greenacres',
    'Pahokee',
    'Palm Beach Gardens',
    'Riviera Beach',
    'South Bay',
    'Lake Worth',
    'West Palm Beach'
);

$BROWARD = array(
    'Coconut Creek',
    'Cooper City',
    'Coral Springs',
    'Dania Beach',
    'Deerfield Beach',
    'Fort Lauderdale',
    'Hallandale Beach',
    'Hollywood',
    'Lauderdale Lakes',
    'Lauderhill',
    'Lighthouse Point',
    'Margate',
    'Miramar',
    'North Lauderdale',
    'Oakland Park',
    'Parkland',
    'Pembroke Pines',
    'Plantation',
    'Pompano Beach',
    'Sunrise',
    'Tamarac',
    'West Park',
    'Weston',
    'Wilton Manors'
);

$DADE = array(
    'Aventura',
    'Coral Gables',
    'Doral',
    'Florida City',
    'Hialeah Gardens',
    'Hialeah',
    'Homestead',
    'Miami',
    'Miami Beach',
    'Miami Gardens',
    'Miami Springs',
    'North Bay Village',
    'North Miami Beach',
    'North Miami',
    'Opa-locka',
    'South Miami',
    'Sunny Isles Beach',
    'Sweetwater',
    'West Miami'
);