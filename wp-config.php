<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, WordPress Language, and ABSPATH. You can find more information
 * by visiting {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'miamirealestate');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'e5>|vJ.7nfnE+r|7H!WIIk8PHP7&ibHL!-9If-GIS0qc-{t<JswXDZyM]B4>^*-1');
define('SECURE_AUTH_KEY',  'aC+C0kgp:n,6iTJs-;-Kt&J_;zF)B]-RD~S2BCtaVI;%z<?@=B4y!QwWkZjG|-r/');
define('LOGGED_IN_KEY',    'rYy.e%=JA-z|^3b>(&u20MD}Gm|rj&;6gH;.EK$%:<oP;M{kfla{p.(_CcFe[g.S');
define('NONCE_KEY',        '85+A%5V}s7RX,P=52seOGV]|b9S`L!R34Thb/[HJRiw`&)W59.0d|2|Dv+[lT>C=');
define('AUTH_SALT',        'GRHr3y[(D{Jw_x1+[m}()C>Wv]0r5[U4vp@-l<+O4dH:+VY6JD>IPb4`|}s`mC5+');
define('SECURE_AUTH_SALT', '-(WBXDgX<yPIdJ-lN-c#>dCRuO_X^hW?1dz]L/S}i!!&26@OZ&Iq;L2,$f9%n{Zz');
define('LOGGED_IN_SALT',   '/Vt?t_x}+/hCQMaGgYt^5*~MicF<x Zf00~4{mC{*=8l=TP&$@/_5:V_Ct]owa7I');
define('NONCE_SALT',       '`*mr/G6Ew9yfK2E/nZE5(]UTUdKs9Iv{i=/! I-VonoLYen:p`8D+E|QY7:_^Wc,');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * WordPress Localized Language, defaults to English.
 *
 * Change this to localize WordPress. A corresponding MO file for the chosen
 * language must be installed to wp-content/languages. For example, install
 * de_DE.mo to wp-content/languages and set WPLANG to 'de_DE' to enable German
 * language support.
 */
define('WPLANG', '');

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

define('FS_METHOD', 'direct');